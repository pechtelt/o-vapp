import com.sun.source.tree.Tree;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class Data {
   private final HashMap<String, Location> locationMap = new HashMap<>();
   private final SortedMap<String, Route> routeMap = new TreeMap<>();

   public Data() {
      /// === Locations A ... F ========

      var location = new Location("A");
      locationMap.put(location.getName(), location);

      location = new Location("B");
      locationMap.put(location.getName(), location);

      location = new Location("C");
      locationMap.put(location.getName(), location);

      location = new Location("D");
      locationMap.put(location.getName(), location);

      location = new Location("E");
      locationMap.put(location.getName(), location);

      location = new Location("F");
      locationMap.put(location.getName(), location);

      ////////////////////////////////////////////////////////////

      /// === Routes A-B-C-D ========
      for (int hour = 7; hour <= 19; hour += 2) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("A"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
         route.addEndPoint(locationMap.get("D"), LocalTime.of(hour, 46));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes D-C-B-A ========
      for (int hour = 7; hour <= 19; hour += 2) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("D"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
         route.addEndPoint(locationMap.get("A"), LocalTime.of(hour, 51));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes E-B-C-F ========
      for (int hour = 8; hour <= 17; hour += 1) {
         var departure = LocalTime.of(hour, 30);
         var route = new Route(locationMap.get("E"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
         route.addEndPoint(locationMap.get("F"), LocalTime.of(hour, 51));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes F-C-B-E ========
      for (int hour = 8; hour <= 17; hour += 1) {
         var departure = LocalTime.of(hour, 30);
         var route = new Route(locationMap.get("F"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
         route.addEndPoint(locationMap.get("E"), LocalTime.of(hour, 51));
         routeMap.put(route.getKey(), route);
      }

      // === Route B-C ===
      for (int hour = 12; hour <= 13; hour += 2) //TODO: Make this one diffrent just one train
      {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("B"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
         routeMap.put(route.getKey(), route);
      }

      // === Route C-B ===
      for (int hour = 12; hour <= 13; hour += 2) //TODO: Make this one diffrent just one train
      {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("C"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
         routeMap.put(route.getKey(), route);
      }
   }

   ////////////////////////////////////////////////////////////

   public void writeAllRoutes()
   {
      int count = 0;
      for (var route : routeMap.values()){
         System.out.printf("%2d. ", ++count );
         route.write();
      }
   }

   public void writeRoutesABCD() {
      int count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var route = e.getValue();
            if (key.contains("A-B-C-D")) {
               System.out.printf("%2d. ", ++count);
               route.write();
            }
      }
   }

   public void writeRoutesBD() {
      int count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var route = e.getValue();

         var posB = key.indexOf( "B" );
         var posD = key.indexOf( "D" );

            if (posD > posB) {
               System.out.printf("%2d. ", ++count);
               route.write();
            }
      }
   }

   public void writeRoutes(String key1, String key2, LocalTime t) {
      int count = 0;
      for (var e : routeMap.entrySet()) {

         var key = e.getKey();
         var route = e.getValue();

         var posB = key.indexOf(key1);
         var posD = key.indexOf(key2);

         // tijd van stopover in punt B moet < dan localtime t

         if (posD > posB) {
            var stop = route.getStopOver(key1);
            var departure = stop.getDeparture();

            if (departure.isAfter(t)) {
               System.out.printf("%2d. ", ++count);
               route.write();
            }
         }
      }
   }
}